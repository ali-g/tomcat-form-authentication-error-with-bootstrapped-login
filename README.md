# Tomcat Form Authentication Error with Bootstrapped Login

example project to display problem:

When running the given demo locally on tomcat the 408 error occurs when using the login.html as login and when using basicLogin there are no problems

the script for the db in the example:

        **CREATE TABLE mytable (
        	x VARCHAR(10),
            y VARCHAR(10),
            z VARCHAR(10)
        );
        INSERT INTO mytable(x,y,z)
        VALUES('A','B','C'),
        	('D', 'E', 'F');**

to adapt home.jsp for deployment:

    edit lines 35 and 36:
        String url = "jdbc:mysql://[host]:[port]/[db]?&serverTimezone=" + TimeZone.getDefault().getID();
    	conn = DriverManager.getConnection(url , "root" , "");
    	
    	- replace [host], [port], [db] and fill in password in 36
