<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.util.*,java.sql.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  		<meta name="description" content="">
  		<meta name="author" content="">
  		<!-- page title - shown in tab -->
  		<title>Home</title>
    </head>
    <body>
  <!-- Page Content -->
  <div class="container" style="padding-top: 120px;">
    <div class="row">
      <div class="col-lg-12 text-center">
      	<table class="table col-sm-12" border="1" style="width:100%">
	           				<tr>
								<td align="center" valign="middle" style="font-size: 16px">x</td>
								<td align="center" valign="middle" style="font-size: 16px">y</td>
								<td align="center" valign="middle" style="font-size: 16px">z</td>
	           				</tr>
	           			    <%
								try
								{
									Connection conn;
									Class.forName("com.mysql.cj.jdbc.Driver");
									String url = "jdbc:mysql://[host]:[port]/[db]?&serverTimezone=" + TimeZone.getDefault().getID();
									conn = DriverManager.getConnection(url , "root" , "");
									//System.out.println(status);
									String query ="SELECT * FROM mytable";
									PreparedStatement st = conn.prepareStatement(query);
									ResultSet rs = st.executeQuery();
									while(rs.next())
									{
									%>
	    								<tr><td><%=rs.getString("x") %></td>
									        <td><%=rs.getString("y") %></td>
										    <td><%=rs.getString("z") %></td>
									   </tr>
	        						<%
									}	
							%>
	    </table>
	    						<%
	    						rs.close();
	    						st.close();
	    						conn.close();
	    						}
							catch(Exception e)
							{
	    						e.printStackTrace();
	    						}
							%>
      </div>
     </div>
  </div>
 </body>
</html>
